import React, { useEffect, useState } from "react";
// import React from "react";
import { success, error } from "../Basic/InformationModal";
import { Form, Input, Button } from "antd";
import { postData } from "../Crud/CUDData";

const FormItem = Form.Item;

// function IsiForm(props) {
//   console.log(props);
//   return null;
// }

// class IsiForm extends React.Component {
//   state = {
//     confirmDirty: false
//   };

function IsiForm(props) {
  const [confirmDirty] = useState(false);
  const { validateFieldsAndScroll } = props.form;
  // console.log(props.URL, props.form);

  // handleSubmit = e => {
  function handleSubmit(e) {
    e.preventDefault();
    // this.props.validateFieldsAndScroll((err, values) => {
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        postData(
          // this.props.URL,
          props.URL,
          values.product_category,
          values.product_name,
          values.product_size,
          values.product_color,
          values.product_price
        );
        success(
          "Sukses",
          "Anda berhasil menambahkan data, refresh page ini melihat hasilnya"
        );
      } else if (err) {
        error("Error", "Cek kembali masukkan Anda");
      }
    });
  }

  // handleConfirmBlur = e => {
  //   const value = e.target.value;
  //   this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  // };

  function handleConfirmBlur(e) {
    const value = e.target.value;
    useState(confirmDirty || !!value);
  }

  // render() {
  //   const { getFieldDecorator } = this.props.form;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 }
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 8 }
    }
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0
      },
      sm: {
        span: 16,
        offset: 8
      }
    }
  };

  const { getFieldDecorator } = props.form;

  return (
    // <Form onSubmit={this.handleSubmit}>
    <Form onSubmit={e => handleSubmit(e)}>
      <FormItem {...formItemLayout} label="Merk Produk">
        {/* samakan field decorator dengan kolom pada tabel */}
        {getFieldDecorator("product_name", {
          rules: [
            {
              required: true,
              message: "Harap isi merk produk!"
            }
          ]
        })(<Input />)}
      </FormItem>
      <FormItem {...formItemLayout} label="Kategori">
        {getFieldDecorator("product_category", {
          rules: [
            {
              required: true,
              message: "Harap isi kategori!"
            }
          ]
        })(<Input />)}
      </FormItem>
      <FormItem {...formItemLayout} label="Ukuran">
        {getFieldDecorator("product_size", {
          rules: [
            {
              required: true,
              message: "Harap isi ukuran!"
            }
          ]
        })(<Input />)}
      </FormItem>
      <FormItem {...formItemLayout} label="Warna">
        {getFieldDecorator("product_color", {
          rules: [
            {
              required: true,
              message: "Harap isi warna!"
            }
          ]
        })(<Input />)}
      </FormItem>
      <FormItem {...formItemLayout} label="Harga">
        {getFieldDecorator("product_price", {
          rules: [
            {
              required: true,
              message: "Harap isi harga!"
            }
          ]
        })(<Input />)}
      </FormItem>
      <FormItem {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit">
          Simpan Data
        </Button>
      </FormItem>
    </Form>
  );
  // }
}

const FormIsiData = Form.create()(IsiForm);

export default FormIsiData;

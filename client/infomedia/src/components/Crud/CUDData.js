import axios from "axios";

export function putData(...props) {
  // deconstruct Array dari rest proops
  const [URL, product_id, newData, index] = props;
  // console.log(URL)
  // console.log('newData', newData[index]) // data I want to update to API
  // console.log('index', index) // index adalah index array saat ini (0)
  // console.log('product_id', product_id) // id adalah nomor index dalam tabel database, jangan sampai tertukar
  // console.log('product_category', newData[index].product_category)
  // console.log('product_name', newData[index].product_name)
  axios.put(URL + `api/product/telkom-update/${product_id}`, {
    product_category: newData[index].product_category,
    product_name: newData[index].product_name,
    product_size: newData[index].product_size,
    product_color: newData[index].product_color,
    product_price: newData[index].product_price
  });
}

export function deleteData(...props) {
  const [URL, product_id] = props;
  axios.delete(URL + `api/product/telkom-delete/${product_id}`);
}

export function postData(...props) {
  const [
    URL,
    product_category,
    product_name,
    product_size,
    product_color,
    product_price
  ] = props;
  axios.post(URL + `api/product/telkom-new`, {
    // posisinya jangan sampai tertukar, sesuaikan dengan backend
    product_category: product_category,
    product_name: product_name,
    product_size: product_size,
    product_color: product_color,
    product_price: product_price
  });
}

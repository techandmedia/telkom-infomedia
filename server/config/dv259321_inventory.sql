-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2018 at 09:53 AM
-- Server version: 5.5.60-MariaDB-cll-lve
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dv259321_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `andri`
--

CREATE TABLE `andri` (
  `id` int(10) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nohp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `andri`
--

INSERT INTO `andri` (`id`, `nim`, `nama`, `alamat`, `email`, `nohp`) VALUES
(1, '12345678', 'GGGG', 'Jak', 'email', '123'),
(2, '161616', 'Andri', 'Jakarta', 'jak@email.com', '123123123'),
(3, '161617', 'Andri', 'Jakarta', 'jak@email.com', '123123123'),
(4, '161618', 'Michael', 'Jombang', 'ngga pnya', '123123123'),
(5, '161619', 'Andri', 'Jakarta', 'jak@email.com', '123123123'),
(6, '161620', 'Andri', 'Jakarta', 'jak@email.com', '123123123'),
(7, '123456', 'Hujan', 'PASARSENEN', 'JAHAUAB@GMAIL.COM', '0546558');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `userid` varchar(10) NOT NULL,
  `usergroup` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `fullname`, `title`, `emailid`, `mobile`, `image`, `userid`, `usergroup`, `created_at`) VALUES
(1, 'Subarnant', 'InternBanget', 'eko.swadharma', '0909090909', '', '', '', '2018-05-30 01:59:17'),
(2, 'Michael Jackson', 'Finance Manager', 'michael.jackson@kemitraan.or.id', '08119090123', 'images/mj.jpg', 'PROJ-12345', 'Finance', '2018-05-30 01:59:17'),
(3, 'Justin Timberlake', 'Project Director', 'justin.timberlake@kemitraan.or.id', '0812367812', 'images/justin.jpg', 'PROJ-12999', 'Project Field', '2018-05-30 01:59:17'),
(4, 'Beyonce Knowles', 'Super Star', 'bey@kemen.or.id', '0813131313', 'images/beyonce.jpg', '', 'Entertainer', '2018-05-30 07:02:36'),
(14, 'Disgaea', 'Director', 'disgaea@games.ps2', '123901290', 'images/disgaea.jpg', 'PROJ-REACT', 'Entertainment', '2018-05-31 10:21:59'),
(15, 'Avengers', 'Super Director', 'Avengers@keren.com', '555555', 'images/avengers.jpg', 'PROJ-REACT', 'Entertainment', '2018-06-11 12:00:27'),
(16, 'Nephews with Grandma', 'Super Director', 'newp@fam.com', '999999', 'images/Nephews.jpeg', 'PROJ-REACT', 'Entertainment', '2018-06-11 12:02:28'),
(17, 'Naruto', 'Director', 'naruto@wow.com', '123901290', 'images/naruto.jpg', 'PROJ-REACT', 'Entertainment', '2018-06-19 09:20:22'),
(18, 'Bimo Sarashadi', 'CEO', 'bi@kement.or.id', '598789', 'images/bim.jpg', 'PROJ-REACT', 'Construction', '2018-06-19 09:21:16'),
(19, 'Luffy', 'Director', 'luffy@one.piece.com', '123901290', 'images/luffy.jpg', 'PROJ-REACT', 'Entertainment', '2018-06-19 02:20:22'),
(20, 'Hulk', 'Director', 'hulk@wow.com', '123901290', 'images/hulk.jpg', 'PROJ-REACT', 'Entertainment', '2018-07-17 04:30:40'),
(33, 'Andri Bos', 'Bos Gede Banget', 'testes', '8090800', '', '', '', '2018-07-17 10:23:33'),
(34, 'Mas Bejo', 'Teknisi Bengkel', 'bejo@bengkel.com', '123456789', '', '', '', '2018-07-18 07:04:37'),
(35, 'dedi', 'pasarseneb', 'oaijo@gmail.com', '87127384', '', '', '', '2018-07-21 03:20:26'),
(36, 'Andri123', 'Direktur Kodok', 'eko.andri@suarnanto', '123123', '', '', '', '2018-07-28 06:15:45'),
(37, 'Thanos', 'Bos Titan', 'thanos@titan.com', '159357', '', '', '', '2018-07-28 23:47:37'),
(38, 'Spiderman', 'Tukang Jaring', 'jaring@hatiku.com', '364364', '', '', '', '2018-07-28 23:48:58'),
(39, 'Andri lagi', 'Ngetes', 'email', '129', '', '', '', '2018-07-30 03:12:20'),
(40, 'Andri2', 'yoi', 'you', '909090', '', '', '', '2018-07-30 03:13:59'),
(41, 'asdas', 'asdadaas', 'dad', '43434', '', '', '', '2018-07-30 03:16:16'),
(42, 'Andri123', '123', '123', '123', '', '', '', '2018-07-30 03:18:48'),
(43, 'Andri lagi', '123', '123', '123', '', '', '', '2018-07-30 03:24:07'),
(44, 'AAAAAAAAA', 'asasas', 'tes123@email.com', '67676', '', '', '', '2018-07-30 03:33:44'),
(45, '123', '123', '123', '123', '', '', '', '2018-07-30 04:23:40'),
(46, 'Zebra', '123', '123', '123', '', '', '', '2018-07-30 04:27:09'),
(47, 'sjdj', 'jdhdh', 'jdhdh', '6448', '', '', '', '2018-07-31 13:56:10'),
(48, 'A123', 'Super', 'Bos@gmail.com', '123', '', '', '', '2018-08-02 03:24:47');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id_dosen` varchar(10) NOT NULL,
  `nama_dosen` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `nama_dosen`, `created`) VALUES
('100', 'Ir. Yogasetya Suhanda, M.Sc', '2018-11-12 12:50:00'),
('111', 'V. Kun Marjonohadi, S.Sos, MM', '2018-11-12 12:10:29'),
('112', 'Nur Sucahyo, S,Si., MM', '2018-11-12 12:11:21'),
('113', 'Jelman Nasri, S.Kom., SE., MM.', '2018-11-12 12:11:21'),
('114', 'Ir. Indra Hiswara, MM', '2018-11-12 12:12:33'),
('115', 'Abdul Aziz Effendy, S.Kom., M.Kom', '2018-11-12 12:12:33'),
('116', 'Heru Winarno, S.Kom., MM', '2018-11-12 12:17:20'),
('117', 'Dhila Franzely Dimas, S.Kom., MM.Si', '2018-11-12 12:17:20'),
('118', 'Abdul Manan, S.Kom., M.Kom', '2018-11-12 12:17:20'),
('119', 'Muhamad Yasin, S.Kom.', '2018-11-12 12:17:20'),
('120', 'Slamet Didik Agus Kurniawan, S.Kom', '2018-11-12 12:17:20'),
('121', 'Dartono, S.Kom., M.Kom', '2018-11-12 12:17:20'),
('122', 'Jody Ridwan Arief, S.Kom', '2018-11-12 12:17:20'),
('123', 'Adi Sopian, S.Kom., M.Kom', '2018-11-12 12:17:20'),
('124', 'Baharini Kurnia Putri, S.Kom., M.Pd.', '2018-11-12 12:17:20'),
('125', 'Usanto, S.Kom., M.Kom', '2018-11-12 12:17:20'),
('126', 'Amirudin Kurdi, S.Ag, MM', '2018-11-12 12:17:20'),
('127', 'Drs. Satrio Broto, MM', '2018-11-12 12:17:20'),
('128', 'Ike Kurniati, S.Kom., M. Kom', '2018-11-12 12:17:20'),
('129', 'Andy Dharmalau, S.Kom, M.Kom', '2018-11-12 12:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `usergroup` varchar(16) NOT NULL,
  `proj_id` varchar(13) NOT NULL,
  `email` varchar(25) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `title` varchar(25) NOT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `fullname`, `usergroup`, `proj_id`, `email`, `mobile`, `title`, `join_date`) VALUES
(1, 'Monica Tanuhandaru', 'Executive Office', '1', '', '', 'Chief Executive Officer', '0000-00-00 00:00:00'),
(2, 'Hindijani Novita', 'CRM', '1', '', '', '', '0000-00-00 00:00:00'),
(3, 'Dewi Rizki', 'SDG', '1', '', '', '', '0000-00-00 00:00:00'),
(4, 'Dewi Rizki', 'SDG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(5, 'Emir Rio Krishna', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(6, 'Emir Rio Krishna', 'Operations', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(7, 'Budi Setiawan', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(8, 'Budi Setiawan', 'Operations', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(9, 'Noviani Utami', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(10, 'Noviani Utami', 'Operations', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(11, 'Jumali', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(12, 'Jumali', 'Operations', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(13, 'Kurniasih Paturahman', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(14, 'Rizki Ramadhano    ', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(15, 'Meisi Subandi', 'Operations', '1', '', '', '', '0000-00-00 00:00:00'),
(16, 'Heny Pratiwi', 'HRGS', '1', '', '', '', '0000-00-00 00:00:00'),
(17, 'Heny Pratiwi', 'HRGS', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(18, 'Titik Wahyuningsih', 'HRGS', '1', '', '', '', '0000-00-00 00:00:00'),
(19, 'Merry Nurrani M.', 'HRGS', '1', '', '', '', '0000-00-00 00:00:00'),
(20, 'Merry Nurrani M.', 'HRGS', '1', '', '', '', '0000-00-00 00:00:00'),
(21, 'Muzakir', 'HRGS', '1', '', '', '', '0000-00-00 00:00:00'),
(22, 'Citra Oktaviani', 'HRGS', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(23, 'Dede Herdiana', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(24, 'Dede Herdiana', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(25, 'Samsul Ridwan (Iwan)', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(26, 'Samsul Ridwan (Iwan)', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(27, 'Samsul Ridwan (Iwan)', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(28, 'Samsul Ridwan (Iwan)', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(29, 'Samsul Ridwan (Iwan)', 'IT', '1', '', '', '', '0000-00-00 00:00:00'),
(30, 'Ayu Prahandini ', 'CRM', '1', '', '', '', '0000-00-00 00:00:00'),
(31, 'Moch Ghufron', 'PME', '1', '', '', '', '0000-00-00 00:00:00'),
(32, 'Moch Ghufron', 'PME', '1', '', '', '', '0000-00-00 00:00:00'),
(33, 'Zikry Auliya', 'PME', '1', '', '', '', '0000-00-00 00:00:00'),
(34, 'Zikry Auliya', 'PME', '1', '', '', '', '0000-00-00 00:00:00'),
(35, 'Inda Loekman', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(36, 'Inda Loekman', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(37, 'Hery Sulistio', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(38, 'Riana Ekawati', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(39, 'Arif Nurdiansah', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(40, 'Arif Nurdiansah', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(41, 'Fawaiq Suwanan', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(42, 'Fawaiq Suwanan', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(43, 'Amalia Fubani', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(44, 'Hana Alfahani', 'KRC', '1', '', '', '', '0000-00-00 00:00:00'),
(45, 'Irfan Nasrullah', 'KRC', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(46, 'Ahmad Qisa\'i', 'CSOG', '1', '', '', '', '0000-00-00 00:00:00'),
(47, 'Ahmad Qisa\'i', 'CSOG', '1', '', '', '', '0000-00-00 00:00:00'),
(48, 'Ahmad Qisa\'i', 'CSOG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(49, 'Refki Saputra', 'CSOG', '1', '', '', '', '0000-00-00 00:00:00'),
(50, 'Rahmi Sosiawaty', 'CSOG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(51, 'Pedro Horas', 'CSOG', '1', '', '', '', '0000-00-00 00:00:00'),
(52, 'Maria Krishnanti', 'CSOG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(53, 'Derosya', 'CSOG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(54, 'Sari Wardhani', 'CSOG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(55, 'Fajarwati', 'CSOG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(56, 'Ririn Sefsani', 'CSOG', '1', '', '', '', '0000-00-00 00:00:00'),
(57, 'Camelia Tri Lestari', 'CSOG', '1', '', '', '', '0000-00-00 00:00:00'),
(58, 'Hasbi Berliani', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(59, 'Hasantoha Adnan ', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(60, 'Abimanyu Sasongko Aji ', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(61, 'Dayu Nirma Amuwanti', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(62, 'Binbin Mariana', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(63, 'Amalia Prameswari', 'SEG', '1', '', '', '', '0000-00-00 00:00:00'),
(64, 'Yunidar', 'SEG', '1', '', '', '', '0000-00-00 00:00:00'),
(65, 'Adetya Rahmi??? ', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(66, 'Irendra Radjawali', 'SEG', '1', '', '', '', '0000-00-00 00:00:00'),
(67, 'Yesaya Hardyanto', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(68, 'Suwito', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(69, 'Gladi Hardyanto', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(70, 'Tuningsih', 'SEG', '1', '', '', '', '0000-00-00 00:00:00'),
(71, 'Endang Habsari', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(72, 'Lensi Rianis ', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(73, 'Elly Yusnawati', 'SEG', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(74, 'Yasir Sani', 'PEEG ', '1', '', '', '', '0000-00-00 00:00:00'),
(75, 'Alexander Mering', 'PEEG ', '1', '', '', '', '0000-00-00 00:00:00'),
(76, 'Widya Anggraini   ', 'PEEG ', 'Not Available', '', '', '', '0000-00-00 00:00:00'),
(77, 'Wahyu Pamuji', 'PEEG ', '1', '', '', '', '0000-00-00 00:00:00'),
(78, 'Efrizal Zein', 'PEEG ', '1', '', '', '', '0000-00-00 00:00:00'),
(79, 'Ina Desilia', 'PEEG ', '1', '', '', '', '0000-00-00 00:00:00'),
(80, 'Yoyok Mahmudin', 'Siap', '234', 'yykmhumdin@gmail.com', '085647646820', 'Oke', '2018-06-28 08:00:45'),
(81, '', '', '', '', '', '', '2018-08-29 10:39:44');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_kuliah`
--

CREATE TABLE `jadwal_kuliah` (
  `id_jadwal_kuliah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_jam_kuliah` tinyint(2) NOT NULL,
  `id_ruangan` tinyint(2) NOT NULL,
  `id_nama_kelas` tinyint(2) NOT NULL,
  `id_mata_kuliah` tinyint(3) NOT NULL,
  `id_dosen` varchar(10) NOT NULL,
  `id_status_kelas` tinyint(2) NOT NULL,
  `id_semester` tinyint(4) NOT NULL,
  `id_program_studi` tinyint(2) NOT NULL,
  `id_jurusan` tinyint(2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jadwal_kuliah`
--

INSERT INTO `jadwal_kuliah` (`id_jadwal_kuliah`, `tanggal`, `id_jam_kuliah`, `id_ruangan`, `id_nama_kelas`, `id_mata_kuliah`, `id_dosen`, `id_status_kelas`, `id_semester`, `id_program_studi`, `id_jurusan`, `created`, `modified`) VALUES
(2, '0000-00-00', 1, 2, 3, 101, '115', 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '0000-00-00', 3, 7, 2, 115, '123', 1, 5, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '0000-00-00', 3, 4, 2, 100, '123', 1, 3, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '0000-00-00', 3, 4, 2, 120, '121', 1, 6, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '2018-11-01', 3, 4, 2, 120, '121', 1, 6, 1, 2, '2018-11-13 09:21:22', '2018-11-13 09:21:22'),
(7, '2018-10-29', 3, 5, 3, 121, '123', 1, 5, 1, 2, '2018-11-14 03:19:45', '2018-11-14 03:19:45'),
(8, '2018-11-14', 1, 1, 2, 101, '118', 2, 1, 1, 1, '2018-11-14 03:33:24', '2018-11-14 03:33:24'),
(9, '2018-11-04', 2, 2, 2, 102, '123', 1, 2, 2, 1, '2018-11-19 11:31:36', '2018-11-19 11:31:36'),
(10, '2018-12-07', 3, 2, 4, 101, '118', 3, 4, 1, 1, '2018-11-30 09:25:06', '2018-11-30 09:25:06'),
(11, '2018-12-01', 1, 7, 3, 117, '128', 2, 5, 1, 1, '2018-12-01 00:44:59', '2018-12-01 00:44:59'),
(12, '2018-12-04', 1, 1, 2, 103, '118', 1, 8, 1, 2, '2018-12-04 00:26:43', '2018-12-04 00:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `jam_kuliah`
--

CREATE TABLE `jam_kuliah` (
  `id_jam_kuliah` tinyint(2) NOT NULL,
  `jam_kuliah_awal` time NOT NULL,
  `jam_kuliah_akhir` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jam_kuliah`
--

INSERT INTO `jam_kuliah` (`id_jam_kuliah`, `jam_kuliah_awal`, `jam_kuliah_akhir`) VALUES
(1, '08:00:00', '12:00:00'),
(2, '13:00:00', '16:30:00'),
(3, '18:00:00', '21:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` tinyint(2) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(3, 'Manajemen Informatika'),
(1, 'Sistem Informasi'),
(2, 'Teknik Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mahasiswa` varchar(10) NOT NULL,
  `nama_mahasiswa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mata_kuliah`
--

CREATE TABLE `mata_kuliah` (
  `id_mata_kuliah` tinyint(3) NOT NULL,
  `nama_mata_kuliah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mata_kuliah`
--

INSERT INTO `mata_kuliah` (`id_mata_kuliah`, `nama_mata_kuliah`) VALUES
(102, 'Akuntansi'),
(101, 'Aljabar Linier'),
(100, 'Data Mining'),
(121, 'E-Business'),
(103, 'Kalkulus'),
(120, 'Kecerdasan Buatan'),
(118, 'Manajemen Basis Data'),
(119, 'Multimedia'),
(117, 'Pemrogaman Berbasis Web'),
(114, 'Sistem Informasi Strategik'),
(115, 'Sistem Operasi'),
(116, 'Struktur Data');

-- --------------------------------------------------------

--
-- Table structure for table `nama_kelas`
--

CREATE TABLE `nama_kelas` (
  `id_nama_kelas` tinyint(2) NOT NULL,
  `nama_kelas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nama_kelas`
--

INSERT INTO `nama_kelas` (`id_nama_kelas`, `nama_kelas`) VALUES
(1, 'Malaka A'),
(2, 'Malaka B'),
(3, 'Malaka C'),
(4, 'Pondok Cabe A'),
(5, 'Pondok Cabe B'),
(6, 'Pondok Cabe C');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `category` varchar(25) NOT NULL,
  `productname` varchar(255) NOT NULL,
  `os` varchar(50) NOT NULL,
  `model` varchar(255) NOT NULL,
  `serialnumber` varchar(255) NOT NULL,
  `price` decimal(13,2) NOT NULL,
  `equipment_condition` varchar(25) NOT NULL,
  `detail` text NOT NULL,
  `image` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category`, `productname`, `os`, `model`, `serialnumber`, `price`, `equipment_condition`, `detail`, `image`, `time`) VALUES
(1, 'Laptop 123', 'Lenovo', 'Windows', 'Super', '127', '5000000000.00', '', '', '', '0000-00-00 00:00:00'),
(2, 'Desktop', 'Kompute 123', 'Linux', '090', '123', '300000.00', 'New', '', '', '0000-00-00 00:00:00'),
(3, 'Desktop', 'Dell Dimension XPS 15', 'Linux', 'Super', '999', '80000000.00', 'New', '', '', '0000-00-00 00:00:00'),
(4, '', 'tes lagi', '', 'oifaslhlh', '80009090', '700000.00', 'new', '', '', '0000-00-00 00:00:00'),
(5, '', 'asafafafa', '', 'sdgsdgsvs', '23232424', '33252552342.00', 'sdfs', 'sdfgsgfdhdhd', '', '0000-00-00 00:00:00'),
(6, '', 'dsfsdsgsgs', '', 'sdfsfsdfs', 'sdgsgsgs', '99999999999.99', 'fssdfs', 'sdfsdfs', '', '0000-00-00 00:00:00'),
(7, '', 'tes123123', '', 'lksjdfsdf', 'kljsdfsd', '9000000.00', 'new', 'ksjdlflsdf', '', '0000-00-00 00:00:00'),
(8, '', 'tes124', '', 'lkjsfs;', 'lkjsdlf', '3443343232.00', 'lkjsvpsd', 'josds', '', '0000-00-00 00:00:00'),
(9, '', 'sadas', '', 'asdasda', 'asad', '600000.00', 'dfgdfg', 'fsfsd', '', '0000-00-00 00:00:00'),
(10, '', 'TesAndri123123', '', 'klasjda', 'asdasd', '80009000.00', 'new', 'oasdllasd', '', '0000-00-00 00:00:00'),
(11, '', 'Tesjam2lewat5', '', 'jl oihohhl', 'asdllj', '88090909090.00', 'jlsdsf', 'lsjdfuilsejr', '', '0000-00-00 00:00:00'),
(12, 'mango', 'tesbuahbuahan', '', 'lkjalsdad', 'alskjd', '80000112.00', 'lksjdf', 'enak', '', '0000-00-00 00:00:00'),
(13, '', 'tes terakhir', '', 'kljlkajsd', 'asdasd', '80232.00', 'askjsal', 'lkjasa', '', '0000-00-00 00:00:00'),
(14, 'coconut', 'tesbuah', '', 'adasd', 'asdasd', '32423424.00', 'sad', 'ewew', '', '2018-06-26 09:05:14'),
(15, 'computer', 'sadsfsfdlk', '', 'kjsdkfjsdf', 'sfsdfuusyu', '9090232090.00', 'kljsdfs', 'lskjdfsf', '', '2018-06-26 11:52:10'),
(16, 'jaringan', 'Network Switch', '', 'jklajalda', '98121982', '8988837190.00', 'New', 'iioioausdlllasd', '', '2018-06-26 12:02:56'),
(17, 'jaringan', 'Cisco', '', '989', '123', '50000.00', 'New', 'Switch', '', '2018-06-26 13:32:57'),
(18, 'jaringan', 'Kabel LAN 841', '', 'jkasda', 'asasd', '3999.00', 'new', 'Ok', '', '2018-06-26 13:42:51'),
(19, 'jaringan', 'Kabel Jaringan 843', '', 'sf', 'd', '9090.00', 'new', 'kjasd', '', '2018-06-26 13:44:38'),
(20, 'jaringan', 'Kabel LAN 844', '', '123', '123', '123.00', 'new', 'ok', '', '2018-06-26 13:45:26'),
(21, 'linux', 'Asus ROG', '', '', '', '0.00', '', '', '', '2018-06-28 04:24:17'),
(22, 'mac', 'Asus', '', '', '', '0.00', '', '', '', '2018-06-28 04:27:40'),
(23, 'mac', 'Macbook Pro', '', '', '', '0.00', '', '', '', '2018-06-28 04:29:52'),
(24, 'Laptop', 'Macbook Pro 307', 'Mac OS', '', '', '0.00', '', '', '', '2018-06-28 08:08:09'),
(25, 'PC Desktop', 'Ubuntu Desktop 315', 'Unix/Linux', '', '', '0.00', '', '', '', '2018-06-28 08:16:36'),
(26, '', '', '', '', '', '0.00', '', '', '', '2018-07-05 13:42:53'),
(27, 'Alat Jaringan', 'Tempat tidur', 'Windows 8/10 Pro', '', '', '0.00', '', '', '', '2018-07-07 16:21:51'),
(28, 'Laptop', 'Laptop Mac rusak', 'Mac OS', '', '', '0.00', '', '', '', '2018-07-07 16:23:19'),
(29, '', '', '', '', '', '0.00', '', '', '', '2018-07-08 21:58:11'),
(30, 'Laptop', 'PakBomo', 'Windows 7 Pro', '', '', '0.00', '', '', '', '2018-07-09 05:02:18'),
(31, '', '', '', '', '', '0.00', '', '', '', '2018-07-09 13:57:18'),
(32, '', '', '', '', '', '0.00', '', '', '', '2018-07-10 02:46:47'),
(33, '', '', '', '', '', '0.00', '', '', '', '2018-07-10 02:46:50'),
(34, '', '', '', '', '', '0.00', '', '', '', '2018-07-10 21:26:54'),
(35, 'Alat Jaringan', 'Laura', 'Windows 7 Pro', '', '', '0.00', '', '', '', '2018-07-14 08:22:02'),
(36, 'Laptop', '', '', '', '', '0.00', '', '', '', '2018-08-10 03:38:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_history`
--

CREATE TABLE `product_history` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `usergroup` varchar(16) NOT NULL,
  `domain` varchar(13) NOT NULL,
  `computertype` varchar(15) NOT NULL,
  `domname` varchar(13) NOT NULL,
  `os` varchar(16) NOT NULL,
  `off` varchar(13) NOT NULL,
  `av` varchar(13) NOT NULL,
  `vpn` varchar(13) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_history`
--

INSERT INTO `product_history` (`id`, `fullname`, `usergroup`, `domain`, `computertype`, `domname`, `os`, `off`, `av`, `vpn`, `date_created`) VALUES
(1, 'Monica Tanuhandaru', 'Executive Office', '1', 'Desktop', 'WS-PGR-004', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(2, 'Hindijani Novita', 'CRM', '1', 'Desktop', 'WS-PGR-015', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(3, 'Dewi Rizki', 'SDG', '1', 'Desktop', 'WS-PGR-019', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(4, 'Dewi Rizki', 'SDG', 'Not Available', 'Macbook', 'Macbook', 'Mac OS', 'Not Available', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(5, 'Emir Rio Krishna', 'Operations', '1', 'Desktop', 'WS-PGR-002', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(6, 'Emir Rio Krishna', 'Operations', 'Not Available', 'Not Available', 'Not Available', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(7, 'Budi Setiawan', 'Operations', '1', 'Desktop', 'WS-PGR-20', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(8, 'Budi Setiawan', 'Operations', 'Not Available', 'Laptop', 'PGR-006', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(9, 'Noviani Utami', 'Operations', '1', 'Desktop', 'WS-PGR-081', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(10, 'Noviani Utami', 'Operations', 'Not Available', 'Laptop', 'PGR-016', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(11, 'Jumali', 'Operations', '1', 'Desktop', 'WS-PGR-009', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(12, 'Jumali', 'Operations', 'Not Available', 'Not Available', 'Not Available', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(13, 'Kurniasih Paturahman', 'Operations', '1', 'Desktop', 'WS-PGR-018', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(14, 'Rizki Ramadhano    ', 'Operations', '1', 'Desktop', 'WS-PGR-013', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(15, 'Meisi Subandi', 'Operations', '1', 'Desktop', 'WS-PGR-008', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(16, 'Heny Pratiwi', 'HRGS', '1', 'Desktop', 'WS-PGR-028', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(17, 'Heny Pratiwi', 'HRGS', 'Not Available', 'Laptop', 'PGR-050', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(18, 'Titik Wahyuningsih', 'HRGS', '1', 'Desktop', 'WS-PGR-005', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(19, 'Merry Nurrani M.', 'HRGS', '1', 'Desktop', 'WS-PGR-011A', 'Windows 7 Pro', 'Office 2016', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(20, 'Merry Nurrani M.', 'HRGS', '1', 'Laptop', 'PGR-039', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(21, 'Muzakir', 'HRGS', '1', 'Desktop', 'WS-PGR-040', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(22, 'Citra Oktaviani', 'HRGS', 'Not Available', 'Laptop', 'PGR-100', 'Windows 8/10 SL', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(23, 'Dede Herdiana', 'IT', '1', 'Desktop', 'WS-PGR-000', 'Windows 8 Pro/10', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(24, 'Dede Herdiana', 'IT', '1', 'Desktop', 'WS-PGR-000', 'Windows 8 Pro/10', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(25, 'Samsul Ridwan (Iwan)', 'IT', '1', 'Desktop', 'WS-PGR-011', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(26, 'Samsul Ridwan (Iwan)', 'IT', '1', 'Laptop', 'PGR-004', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(27, 'Samsul Ridwan (Iwan)', 'IT', '1', 'Laptop', 'PGR-007', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(28, 'Samsul Ridwan (Iwan)', 'IT', '1', 'Laptop', 'PGR-022', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(29, 'Samsul Ridwan (Iwan)', 'IT', '1', 'Laptop', 'PGR-023', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(30, 'Ayu Prahandini ', 'CRM', '1', 'Laptop', 'PGR-029', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(31, 'Moch Ghufron', 'PME', '1', 'Desktop', 'WS-PGR-012', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(32, 'Moch Ghufron', 'PME', '1', 'Laptop', 'PGR-020', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(33, 'Zikry Auliya', 'PME', '1', 'Desktop', 'WS-PGR-036', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(34, 'Zikry Auliya', 'PME', '1', 'Laptop', 'PGR-003', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(35, 'Inda Loekman', 'KRC', '1', 'Desktop', 'WS-PGR-058', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(36, 'Inda Loekman', 'KRC', '1', 'Laptop', 'PGR-027', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(37, 'Hery Sulistio', 'KRC', '1', 'Laptop', 'PGR-016', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(38, 'Riana Ekawati', 'KRC', '1', 'Desktop', 'WS-PGR-060', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(39, 'Arif Nurdiansah', 'KRC', '1', 'Desktop', 'WS-PGR-054', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(40, 'Arif Nurdiansah', 'KRC', '1', 'Laptop', 'PGR-021', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(41, 'Fawaiq Suwanan', 'KRC', '1', 'Desktop', 'WS-PGR-050', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(42, 'Fawaiq Suwanan', 'KRC', '1', 'Laptop', 'PGR-022', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(43, 'Amalia Fubani', 'KRC', '1', 'Desktop', 'WS-PGR-051', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(44, 'Hana Alfahani', 'KRC', '1', 'Desktop', 'WS-PGR-052', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(45, 'Irfan Nasrullah', 'KRC', 'Not Available', 'Not Available', 'Not Available', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(46, 'Ahmad Qisa\'i', 'CSOG', '1', 'Desktop', 'WS-PGR-014', 'Windows 7 Pro', 'Office 2010', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(47, 'Ahmad Qisa\'i', 'CSOG', '1', 'testing', 'Not Available', 'Windows 8 Pro/10', 'Office 2016', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(48, 'Ahmad Qisa\'i', 'CSOG', 'Not Available', 'Not Available', 'Not Available', 'Windows 8/10 SL', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(49, 'Refki Saputra', 'CSOG', '1', 'Laptop', 'PGR-055', 'Windows 8 Pro/10', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(50, 'Rahmi Sosiawaty', 'CSOG', 'Not Available', 'Laptop', 'PGR-040', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(51, 'Pedro Horas', 'CSOG', '1', 'Laptop', 'PGR-030', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(52, 'Maria Krishnanti', 'CSOG', 'Not Available', 'Laptop', 'PGR-', 'Windows 8/10 SL', 'Office 2010', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(53, 'Derosya', 'CSOG', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(54, 'Sari Wardhani', 'CSOG', 'Not Available', 'Laptop', 'PGR-', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(55, 'Fajarwati', 'CSOG', 'Not Available', 'Laptop', 'PGR-047', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(56, 'Ririn Sefsani', 'CSOG', '1', 'Desktop', 'WS-PGR-003', 'Windows 7 Pro', 'Office 2013', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(57, 'Camelia Tri Lestari', 'CSOG', '1', 'Laptop', 'PGR-009', 'Windows 8/10 SL', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(58, 'Hasbi Berliani', 'SEG', 'Not Available', 'Laptop', 'PGR-055', 'Windows 8 Pro/10', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(59, 'Hasantoha Adnan ', 'SEG', 'Not Available', 'Laptop', 'PGR-053', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(60, 'Abimanyu Sasongko Aji ', 'SEG', 'Not Available', 'Laptop', 'PGR-077', 'Not Available', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(61, 'Dayu Nirma Amuwanti', 'SEG', 'Not Available', 'Macbook', 'Macbook', 'Mac OS', 'Not Available', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(62, 'Binbin Mariana', 'SEG', 'Not Available', 'Macbook', 'Macbook', 'Mac OS', 'Not Available', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(63, 'Amalia Prameswari', 'SEG', '1', 'Laptop', 'PGR-056', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(64, 'Yunidar', 'SEG', '1', 'Laptop', 'PGR-028', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(65, 'Adetya Rahmi??? ', 'SEG', 'Not Available', 'Laptop', 'PGR-051', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(66, 'Irendra Radjawali', 'SEG', '1', 'Laptop', 'PGR-019', 'Windows 8 Pro/10', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(67, 'Yesaya Hardyanto', 'SEG', 'Not Available', 'Laptop', 'PGR-088', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(68, 'Suwito', 'SEG', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(69, 'Gladi Hardyanto', 'SEG', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(70, 'Tuningsih', 'SEG', '1', 'Laptop', 'PGR-041', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(71, 'Endang Habsari', 'SEG', 'Not Available', 'Laptop', 'PGR-041', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(72, 'Lensi Rianis ', 'SEG', 'Not Available', 'Laptop', 'PGR-042', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(73, 'Elly Yusnawati', 'SEG', 'Not Available', 'Laptop', 'PGR-043', 'Windows 8/10 SL', 'Office 2013', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(74, 'Yasir Sani', 'PEEG ', '1', 'Desktop', 'WS-PGR-080', 'Windows 8 Pro/10', 'Office 2016', 'Installed', 'Not Available', '0000-00-00 00:00:00'),
(75, 'Alexander Mering', 'PEEG ', '1', 'Laptop', 'PGR-017', 'Windows 8 Pro/10', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(76, 'Widya Anggraini   ', 'PEEG ', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', 'Not Available', '0000-00-00 00:00:00'),
(77, 'Wahyu Pamuji', 'PEEG ', '1', 'Laptop', 'PGR-085', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(78, 'Efrizal Zein', 'PEEG ', '1', 'Laptop', 'PGR-018', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(79, 'Ina Desilia', 'PEEG ', '1', 'Laptop', 'PGR-024', 'Windows 7 Pro', 'Office 2016', 'Installed', 'Installed', '0000-00-00 00:00:00'),
(80, '', '', '', 'Testing 2', '', '', '', '', '', '0000-00-00 00:00:00'),
(81, 'This is a test', 'This is a test', '1', 'Testing', 'This is a tes', 'Windows 8 Pro/10', 'Office 2016', 'Not Available', 'Not Available', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `program_studi`
--

CREATE TABLE `program_studi` (
  `id_program_studi` tinyint(2) NOT NULL,
  `program_studi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program_studi`
--

INSERT INTO `program_studi` (`id_program_studi`, `program_studi`) VALUES
(2, 'Diploma 3'),
(1, 'Strata 1');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` tinyint(2) NOT NULL,
  `nama_role` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'Admin'),
(4, 'Calon Mahasiswa'),
(2, 'Dosen'),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `ruangan_kuliah`
--

CREATE TABLE `ruangan_kuliah` (
  `id_ruangan` tinyint(2) NOT NULL,
  `no_ruangan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ruangan_kuliah`
--

INSERT INTO `ruangan_kuliah` (`id_ruangan`, `no_ruangan`) VALUES
(1, '201'),
(2, '202'),
(3, '203'),
(4, '204'),
(5, '205'),
(6, '206'),
(7, '207'),
(9, '301'),
(10, '302'),
(11, '303'),
(8, '304'),
(12, 'LAB A'),
(13, 'LAB B'),
(14, 'LAB C'),
(15, 'LAB D');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id_semester` tinyint(4) NOT NULL,
  `semester` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_semester`, `semester`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `status_kelas`
--

CREATE TABLE `status_kelas` (
  `id_status_kelas` tinyint(2) NOT NULL,
  `status_kelas` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_kelas`
--

INSERT INTO `status_kelas` (`id_status_kelas`, `status_kelas`) VALUES
(1, 'Hadir'),
(3, 'Kelas Selesai'),
(2, 'Tidak hadir'),
(4, 'Tugas'),
(6, 'UAS'),
(5, 'UTS');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `role`, `email`, `password`, `created`, `modified`) VALUES
(0, 'fauziah', 'jenner', 'Calon Mahasiswa', 'nuradindafauziah1234@gmail.com', 'rahasia4444', '2018-11-14 11:08:18', '2018-11-14 11:08:18'),
(1, 'Eko', 'Andri', 'Mahasiswa', 'and@swa.com', '123', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Adi', 'Sopian', 'Dosen', 'adi@swa.com', '123', '2018-10-18 15:23:39', '2018-10-18 15:23:39'),
(3, 'Admin', 'Admin', 'Admin', 'admin@swa.com', '123', '2018-10-18 15:50:45', '2018-10-18 15:50:45'),
(4, 'use1234', 'asdf', '', 'tes@example.com', 'pass123', '2018-10-18 15:51:08', '2018-10-18 15:51:08'),
(5, 'use1234', 'asdf', '', 'tes1@example.com', 'pass123', '2018-10-18 15:59:35', '2018-10-18 15:59:35'),
(6, 'use1234', 'asdf', '', 'tes12@example.com', 'pass123', '2018-10-18 16:00:14', '2018-10-18 16:00:14'),
(7, 'use1234', 'asdf', '', 'tes123@example.com', 'pass123', '2018-10-18 16:02:05', '2018-10-18 16:02:05'),
(8, 'use1234', 'asdf', '', 'tes124@example.com', 'pass123', '2018-10-18 16:05:46', '2018-10-18 16:05:46'),
(9, 'use1234', 'asdf', '', 'tes125@example.com', 'pass123', '2018-10-18 17:38:54', '2018-10-18 17:38:54'),
(10, 'use1234', 'asdf', '', 'tes126@example.com', 'pass123', '2018-10-18 19:13:46', '2018-10-18 19:13:46'),
(11, 'use1234', 'asdf', '', 'tes132@example.com', 'pass123', '2018-10-18 20:48:53', '2018-10-18 20:48:53'),
(12, 'use1234', 'asdf', 'student', 'tes@com', '123', '2018-10-18 21:53:00', '2018-10-18 21:53:00'),
(13, 'use1234', 'asdf', 'student', 'tes@123com', '123', '2018-10-24 07:31:43', '2018-10-24 07:31:43'),
(14, 'use1234', 'asdf', 'student', 'tes@123123com', '123', '2018-10-24 07:31:58', '2018-10-24 07:31:58'),
(15, 'tes-1234', 'sama', 'student', 'tes@example.123com', 'pass123', '2018-10-24 07:51:49', '2018-10-24 07:51:49'),
(16, 'tes-1234', 'sama', 'student', 'tes@example123com', 'pass123', '2018-10-24 08:10:27', '2018-10-24 08:10:27'),
(17, 'use1234', 'asdf', 'student', 'tes126@ecom', 'pass123', '2018-10-24 08:12:59', '2018-10-24 08:12:59'),
(18, 'use1234', 'asdf', 'student', 'te1s@com', '123', '2018-10-24 08:14:56', '2018-10-24 08:14:56'),
(19, 'use1234', 'asdf', 'student', 'tes111@com', '123', '2018-10-24 08:23:50', '2018-10-24 08:23:50'),
(20, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@1cloud.com', '123', '2018-10-24 08:39:16', '2018-10-24 08:39:16'),
(21, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@2icloud.com', '123', '2018-10-24 08:40:33', '2018-10-24 08:40:33'),
(22, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com1', '123', '2018-10-24 08:50:24', '2018-10-24 08:50:24'),
(23, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com124', '123', '2018-10-24 08:51:12', '2018-10-24 08:51:12'),
(24, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com125', '123', '2018-10-24 08:52:30', '2018-10-24 08:52:30'),
(25, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com126', '123', '2018-10-24 08:54:26', '2018-10-24 08:54:26'),
(26, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com122', '123', '2018-10-24 08:58:16', '2018-10-24 08:58:16'),
(27, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com121', '123', '2018-10-24 08:59:47', '2018-10-24 08:59:47'),
(28, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com127', '123', '2018-10-24 09:27:00', '2018-10-24 09:27:00'),
(29, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com128', '123', '2018-10-24 09:28:52', '2018-10-24 09:28:52'),
(30, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com129', '123', '2018-10-24 09:29:59', '2018-10-24 09:29:59'),
(31, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com123', '123', '2018-10-24 09:30:43', '2018-10-24 09:30:43'),
(32, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com120', '123', '2018-10-24 10:32:04', '2018-10-24 10:32:04'),
(33, '123', '123', 'ad', '123', 'pass123123', '2018-10-24 14:33:29', '2018-10-24 14:33:29'),
(34, 'asasasasa', 'asa', 'Dosen', 'asa', 'asa', '2018-10-24 16:18:58', '2018-10-24 16:18:58'),
(35, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com112', '123', '2018-10-24 17:05:42', '2018-10-24 17:05:42'),
(36, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com1211', '123', '2018-10-28 13:44:32', '2018-10-28 13:44:32'),
(37, 'Andri', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud.com', '123', '2018-11-03 21:52:02', '2018-11-03 21:52:02'),
(38, 'Eko', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud999.com', '123', '2018-11-13 22:46:53', '2018-11-13 22:46:53'),
(39, 'Eko', 'Subarnanto', 'Mahasiswa', 'eko.andri@icloud99.com', '123', '2018-11-13 22:57:42', '2018-11-13 22:57:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `andri`
--
ALTER TABLE `andri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id_dosen`),
  ADD UNIQUE KEY `id_dosen` (`id_dosen`),
  ADD UNIQUE KEY `nama_dosen` (`nama_dosen`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_kuliah`
--
ALTER TABLE `jadwal_kuliah`
  ADD PRIMARY KEY (`id_jadwal_kuliah`),
  ADD KEY `id_status_kelas` (`id_status_kelas`),
  ADD KEY `id_jam_kuliah` (`id_jam_kuliah`),
  ADD KEY `id_nama_kelas` (`id_nama_kelas`),
  ADD KEY `id_mata_kuliah` (`id_mata_kuliah`),
  ADD KEY `id_ruangan` (`id_ruangan`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `id_dosen` (`id_dosen`),
  ADD KEY `id_program_studi` (`id_program_studi`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `jam_kuliah`
--
ALTER TABLE `jam_kuliah`
  ADD PRIMARY KEY (`id_jam_kuliah`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`),
  ADD UNIQUE KEY `nama_jurusan` (`nama_jurusan`);

--
-- Indexes for table `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`id_mata_kuliah`),
  ADD UNIQUE KEY `id_mata_kuliah` (`id_mata_kuliah`),
  ADD UNIQUE KEY `nama_mata_kuliah` (`nama_mata_kuliah`);

--
-- Indexes for table `nama_kelas`
--
ALTER TABLE `nama_kelas`
  ADD PRIMARY KEY (`id_nama_kelas`),
  ADD UNIQUE KEY `id_kelas` (`id_nama_kelas`),
  ADD UNIQUE KEY `nama_kelas` (`nama_kelas`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_history`
--
ALTER TABLE `product_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`id_program_studi`),
  ADD UNIQUE KEY `program_studi` (`program_studi`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`),
  ADD UNIQUE KEY `nama_role` (`nama_role`);

--
-- Indexes for table `ruangan_kuliah`
--
ALTER TABLE `ruangan_kuliah`
  ADD PRIMARY KEY (`id_ruangan`),
  ADD UNIQUE KEY `no_ruangan` (`no_ruangan`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `status_kelas`
--
ALTER TABLE `status_kelas`
  ADD PRIMARY KEY (`id_status_kelas`),
  ADD UNIQUE KEY `status_kelas` (`status_kelas`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `andri`
--
ALTER TABLE `andri`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `jadwal_kuliah`
--
ALTER TABLE `jadwal_kuliah`
  MODIFY `id_jadwal_kuliah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `jam_kuliah`
--
ALTER TABLE `jam_kuliah`
  MODIFY `id_jam_kuliah` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  MODIFY `id_mata_kuliah` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `nama_kelas`
--
ALTER TABLE `nama_kelas`
  MODIFY `id_nama_kelas` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `product_history`
--
ALTER TABLE `product_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `program_studi`
--
ALTER TABLE `program_studi`
  MODIFY `id_program_studi` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ruangan_kuliah`
--
ALTER TABLE `ruangan_kuliah`
  MODIFY `id_ruangan` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
